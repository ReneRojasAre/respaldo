let articulos = [
    {
        "id": 1,
        "nombre": "citroen",
        "img": "https://eldiariony.files.wordpress.com/2017/12/screen-shot-2017-12-29-at-18-49-32.png?w=888",
        "precio": 12999998,
        "cantidad": 1,
        "descripcion": "auto deportivo hibrido"
    },
    {
        "id": 2,
        "nombre": "citroen 2",
        "img": "https://eldiariony.files.wordpress.com/2017/12/screen-shot-2017-12-29-at-18-49-32.png?w=888",
        "precio": 10999998,
        "cantidad": 1,
        "descripcion": "auto deportivo hibrido 2"
    },
    {
        "id": 3,
        "nombre": "citroen 3",
        "img": "https://eldiariony.files.wordpress.com/2017/12/screen-shot-2017-12-29-at-18-49-32.png?w=888",
        "precio": 6999998,
        "cantidad": 1,
        "descripcion": "auto deportivo hibrido 3"
    },
    {
        "id": 4,
        "nombre": "citroen 4",
        "img": "https://eldiariony.files.wordpress.com/2017/12/screen-shot-2017-12-29-at-18-49-32.png?w=888",
        "precio": 15559998,
        "cantidad": 1,
        "descripcion": "auto deportivo hibrido 4"
    }
];


let carrito = {
    "articulos": [],
    "total": 0
};

// const traerListaArticulos = () => {
//     return articulos;
// };

const traerArticulosPorId = (id) => {
    let articuloRetorno = null;
    articulos.forEach(articulos => {
        if (articulos.id == id) {
            articuloRetorno = articulos;
        }
    });
    return articuloRetorno;
};

const agregarCarritoACarrito = (id) => {
    carrito.articulos.push(traerArticulosPorId(id));
}

const entregaPosicionArticulosArregloEnCarritoPorId = (id) => {
    let indiceRetornar = -1;
    carrito.articulos.forEach((articulos, indice) => {
        if (articulos.id == id) {
            indiceRetornar = indice;
        }
    });
    return indiceRetornar;
}

const eliminarCarrito = (id) => {
    carrito.articulos.splice(entregaPosicionArticulosArregloEnCarritoPorId(id), 1);
}

const calcularTotalCompra = () => {
    let sumaTotalRetornar = 0;
    carrito.articulos.forEach((articulos) => {
        sumaTotalRetornar += articulos.precio * articulos.cantidad;
    })
    return sumaTotalRetornar;
}

// console.log(carrito);
// console.log(agregarCarritoACarrito(4));
// console.log(agregarCarritoACarrito(2));
// console.log(carrito);
// eliminarCarrito(2);
// console.log(carrito);

const generarHtmlArticulos = () => {
    let contenido = document.getElementById("contenido");
    let html = "";

    articulos.forEach(articulos => {
        html += "<div>";
        html += "<img src='" + articulos.img + "' onclick='generarModalDetalleArticulo(" + articulos.id + ")' />";
        html += articulos.nombre;
        html += "<div class='hide' >" + articulos.descripcion + "</div>"
        html += "<div class='hide' >" + articulos.precio + "</div>"
        html += "</div>";
    });

    // console.log(contenido);

    contenido.innerHTML = html;
}


const generarModalDetalleArticulo = (id) => {
    let modalArticulo = document.getElementById("modalArticulo");
    let articulo = traerArticulosPorId(id);
    let html = "";

    html += "<div>";
    html += "<img src='" + articulos.img + ")' />";
    html += articulos.nombre;
    html += "<div class='hide' >" + articulos.descripcion + "</div>"
    html += "<div class='hide' >" + articulos.precio + "</div>"
    html += "<input type='button'  onclick='agregarCarritoACarrito(" + id + ")' value='Agregar al carro'/>"
    html += "</div>";

    modalArticulo.innerHTML = html;

}


const generarHtmlCarritoCompras = () => {
    let carritoHtml = document.getElementById("carrito");
    let html = "";
    html += "<table>";
    html += "<tr>";
    html += "<td>Nombre</td>";
    html += "<td>Cantidad</td>";
    html += "<td>precio</td>";
    html += "<td>Total</td>";
    html += "</tr>";

    carrito.articulos.forEach(articulos => {

        html += "<tr>";
        html += "<td>" + articulos.nombre + "</td>";
        html += "<td>" + articulos.cantidad + "</td>";
        html += "<td>" + articulos.precio + "</td>";
        html += "<td>" + articulos.cantidad * articulo.precio + "</td>";
        html += "</td>";
        html += "</tr>";

    });

    html += "</table>";
    carritoHtml.innerHTML = html;
}
