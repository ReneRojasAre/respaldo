let json = {

    "nombre": "",
    "apellidoPaterno": "",
    "apellidoMaterno": "",
    "email": "",
    "razon": "",
    "consulta": ""

}

function rescatarFormularioParaTransformarJson() {

    let pre = document.getElementById("json");
    pre.classList.remove("hide");
    json.nombre = document.getElementById("nombre").value;
    json.apellidoPaterno = document.getElementById("apellidoPaterno").value;
    json.apellidoMaterno = document.getElementById("apellidoMaterno").value;
    json.email = document.getElementById("email").value;
    json.razon = document.getElementById("razon").value;
    json.consulta = document.getElementById("consulta").value;
    // pre.classList.add("PopPup");
    // document.getElementsByName("hobbies").forEach(function (element) {
    //     json.hobbies.push(element.checked);
    // });
    pre.classList.add("PopPup");
    pre.innerHTML = JSON.stringify(json);
    pre.innerHTML = JSON.stringify("Nombre: " + json.nombre) + "<br>" +
        JSON.stringify("Apellido Paterno: " + json.apellidoPaterno) + "<br>" +
        JSON.stringify("Apellido Materno: " + json.apellidoMaterno) + "<br>" +
        JSON.stringify("Email: " + json.email) + "<br>" +
        JSON.stringify("Razón: " + json.razon) + "<br>" +
        JSON.stringify("Consulta: " + json.consulta);

    ValidacionNombre();
    ApellidoPaterno();
    ApellidoMaterno();
    ValidarMail();
    TextConsulta();

}


function ValidacionNombre() {

    valor = document.getElementById("nombre").value;

    var espacios = false;
    var cont = 0;

    while (!espacios && (cont < valor.length)) {
        if (valor.charAt(cont) == " ") {
            espacios = true;
        }
        cont++; // Esto sí ocurre 
    }

    if (valor == null) {
        alert("El campo nombre no puede ser nulo");
        return false;
    } else if (valor.length == 0) {
        alert("Tiene que completar el campo nombre");
        return false;
    } else if (/^\s+$/.test(valor.length)) {
        alert("El campo nombre no esta de forma correcta");
        return false;
    } else if (espacios) {
        alert("el nombre no puede contener espacios en blanco");
        return false;
    }


}

function soloLetras(e) {
    valor = document.getElementById("nombre").value;
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = "8-37-39-46";


    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;

        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        alert("solo debe ingresar letras");
        return false;

    }


}

function ApellidoMaterno() {
    valor = document.getElementById("apellidoMaterno").value;

    var espacios = false;
    var cont = 0;

    while (!espacios && (cont < valor.length)) {
        if (valor.charAt(cont) == " ") {
            espacios = true;
        }
        cont++; // Esto sí ocurre 
    }

    if (valor == null) {
        alert("El campo Apellido Materno no puede ser nulo");
        return false;
    } else if (valor.length == 0) {
        alert("Tiene que completar el campo Apellido Materno");
        return false;
    } else if (/^\s+$/.test(valor.length)) {
        alert("El campo Apellido Materno no esta de forma correcta");
        return false;
    } else if (espacios) {
        alert("el Apellido Materno no puede contener espacios en blanco");
        return false;
    }
}

function ApellidoPaterno() {
    valor = document.getElementById("apellidoPaterno").value;

    var espacios = false;
    var cont = 0;

    while (!espacios && (cont < valor.length)) {
        if (valor.charAt(cont) == " ") {
            espacios = true;
        }
        cont++; // Esto sí ocurre 
    }

    if (valor == null) {
        alert("El campo Apellido Paterno no puede ser nulo");
        return false;
    } else if (valor.length == 0) {
        alert("Tiene que completar el campo Apellido Paterno");
        return false;
    } else if (/^\s+$/.test(valor.length)) {
        alert("El campo Apellido Paterno no esta de forma correcta");
        return false;
    } else if (espacios) {
        alert("el Apellido Paterno no puede contener espacios en blanco");
        return false;
    }
}

function TextConsulta() {
    let valor = document.getElementById("consulta").value;
    if (valor == null) {
        alert("La consulta no puede quedar nula");
        return false;
    } else if (valor.length == 0) {
        alert("Tiene que completar el campo consulta");
        return false;
    } else if (/^\s+$/.test(valor.length)) {
        alert("El campo consulta no esta de forma correcta");
        return false;
    }
    if (valor.length == 500) {
        alert("La consulta no debe tener mas de 500 caracteres");
    }

}

function ValidarMail() {
    valor = document.getElementById("email").value;

    var espacios = false;
    var cont = 0;

    while (!espacios && (cont < valor.length)) {
        if (valor.charAt(cont) == " ") {
            espacios = true;
        }
        cont++; // Esto sí ocurre 
    }

    if (valor == null) {
        alert("El email no puede ser nulo");
        return false;
    } else if (valor.length == 0) {
        alert("Tiene que completar el campo email");
        return false;
    } else if (/^\s+$/.test(valor.length)) {
        alert("El campo email no esta de forma correcta");
        return false;
    } else if (espacios) {
        alert("El email no puede contener espacios en blanco");
        return false;
    }


    let c = 0;
    let p = 0;
    for (let i = 1; i < valor.length; i++) {
        if(valor.charAt(i-1)=="@"){
            c++;
        }

        if(c==1){
            if(valor.charAt(i-1)=="."){
                p++;
            }
        }      
    }

    if(c==1 && p==2 || p==1){
        return true;
    }else{
        alert("Email no Válido");
    }

}
