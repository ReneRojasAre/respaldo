let json = {

    "nombre": "",
    "apellidoPaterno": "",
    "apellidoMaterno": "",
    "edad": 0,
    "email": "",
    "sexo": "",
    "hobbies": [],
    "futbol": ""

}
//variable para realizar seleccion del radio button
//var radioFutbolSeleccionado="";



function rescatarFormularioParaTransformarJson() {

    // let popup = document.getElementById("popupPersona");
    let pre = document.getElementById("json");
    pre.classList.remove("hide");
    json.nombre = document.getElementById("nombre").value;
    json.apellidoPaterno = document.getElementById("apellidoPaterno").value;
    json.apellidoMaterno = document.getElementById("apellidoMaterno").value;
    json.edad = document.getElementById("edad").value;
    json.email = document.getElementById("email").value;
    json.sexo = document.getElementById("sexo").value;
    document.getElementsByName("hobbies").forEach(function (element) {
        json.hobbies.push(element.checked);
    });
    json.futbol = document.querySelector("[name=futbol]:checked").value;
    // let futbolElement = document.querySelector("[name=futbol]:checked");
    pre.classList.add("PopPup");
    // console.log(futbolElement);
    //otra forma de hacer lo del radio button
    //json.futbol = radioFutbolSeleccionado;

    //document.getElementById("CargaDatosJsonModal").style.display = 'block';
    // document.getElementById('fade').style.display='block'
    // pre.innerHTML = JSON.stringify(json);
    pre.innerHTML = JSON.stringify("Nombre: " + json.nombre) + "<br>" +
        JSON.stringify("Apellido Paterno: " + json.apellidoPaterno)+ "<br>"+
        JSON.stringify("Apellido Materno: " + json.apellidoMaterno)+ "<br>"+
        JSON.stringify("Edad: " + json.edad)+ "<br>"+
        JSON.stringify("Sexo: " + json.sexo)+ "<br>"+
        JSON.stringify("Email: " + json.email)+ "<br>"+
        JSON.stringify("Hobbies: " + json.hobbies)+ "<br>"+
        JSON.stringify("¿Le gusta el Fútbol?: " + json.futbol)+ "<br>"
        ;

    ValidacionNombre();
    ApellidoMaterno();
    ApellidoPaterno();
    validarEntero();

}

//metodo para obtener seleccion del radio button
// function guardarSeleccionDeRadioFutbol(elemento){
//     radioFutbolSeleccionado = elemento.value;
//     console.log(radioFutbolSeleccionado);
// }


function ValidacionNombre() {

    valor = document.getElementById("nombre").value;

    var espacios = false;
    var cont = 0;

    while (!espacios && (cont < valor.length)) {
        if (valor.charAt(cont) == " ") {
            espacios = true;
        }
        cont++; // Esto sí ocurre 
    }

    if (valor == null) {
        alert("El campo nombre no puede ser nulo");
        return false;
    } else if (valor.length == 0) {
        alert("Tiene que completar el campo nombre");
        return false;
    } else if (/^\s+$/.test(valor.length)) {
        alert("El campo nombre no esta de forma correcta");
        return false;
    } else if (espacios) {
        alert("el nombre no puede contener espacios en blanco");
        return false;
    }

    if (soloLetras(event) == true) {
        alert("solo debe");
    }
}

function soloLetras(e) {
    valor = document.getElementById("nombre").value;
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = "8-37-39-46";

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }

}

function ApellidoMaterno() {
    valor = document.getElementById("apellidoMaterno").value;

    var espacios = false;
    var cont = 0;

    while (!espacios && (cont < valor.length)) {
        if (valor.charAt(cont) == " ") {
            espacios = true;
        }
        cont++; // Esto sí ocurre 
    }

    if (valor == null) {
        alert("El campo Apellido Materno no puede ser nulo");
        return false;
    } else if (valor.length == 0) {
        alert("Tiene que completar el campo Apellido Materno");
        return false;
    } else if (/^\s+$/.test(valor.length)) {
        alert("El campo Apellido Materno no esta de forma correcta");
        return false;
    } else if (espacios) {
        alert("el Apellido Materno no puede contener espacios en blanco");
        return false;
    }
}

function ApellidoPaterno() {
    valor = document.getElementById("apellidoPaterno").value;

    var espacios = false;
    var cont = 0;

    while (!espacios && (cont < valor.length)) {
        if (valor.charAt(cont) == " ") {
            espacios = true;
        }
        cont++; // Esto sí ocurre 
    }

    if (valor == null) {
        alert("El campo Apellido Paterno no puede ser nulo");
        return false;
    } else if (valor.length == 0) {
        alert("Tiene que completar el campo Apellido Paterno");
        return false;
    } else if (/^\s+$/.test(valor.length)) {
        alert("El campo Apellido Paterno no esta de forma correcta");
        return false;
    } else if (espacios) {
        alert("el Apellido Paterno no puede contener espacios en blanco");
        return false;
    }
}

function validarEntero(valor) {
    //intento convertir a entero. 
    //si era un entero no le afecta, si no lo era lo intenta convertir 
    valor = parseInt(document.getElementById("edad").value);

    //Compruebo si es un valor numérico 
    if (isNaN(valor)) {
        //entonces (no es numero) devuelvo el valor cadena vacia 
        alert("el valor debe ser numerico");
        return false;
    } else {
        //En caso contrario (Si era un número) devuelvo el valor 
        return valor
    }
}

