let json = {

    "nombre": "",
    "apellidoPaterno": "",
    "apellidoMaterno": "",
    "edad": 0,
    "email": "",
    "sexo": "",
    "hobbies": [],
    "futbol": ""

}
//variable para realizar seleccion del radio button
//var radioFutbolSeleccionado="";



function rescatarFormularioParaTransformarJson() {
    
    // let popup = document.getElementById("popupPersona");
    let pre = document.getElementById("json");
    pre.classList.remove("hide");
    json.nombre = document.getElementById("nombre").value;
    json.apellidoPaterno = document.getElementById("apellidoPaterno").value;
    json.apellidoMaterno = document.getElementById("apellidoMaterno").value;
    json.edad = document.getElementById("edad").value;
    json.email = document.getElementById("email").value;
    json.sexo = document.getElementById("sexo").value;
    document.getElementsByName("hobbies").forEach(function (element) {
        json.hobbies.push(element.checked);
    });
    //json.futbol = document.querySelector("[name=futbol]:checked");
    let futbolElement = document.querySelector("[name=futbol]:checked");
    pre.classList.add("PopPup");

    console.log(futbolElement);
    //otra forma de hacer lo del radio button
    //json.futbol = radioFutbolSeleccionado;


    // pre.innerHTML = JSON.stringify(json);
    // ValidacionNombre();
    // ApellidoMaterno();
    // ApellidoPaterno();
    // validarEntero();

    // sololetras();

}

//metodo para obtener seleccion del radio button
// function guardarSeleccionDeRadioFutbol(elemento){
//     radioFutbolSeleccionado = elemento.value;
//     console.log(radioFutbolSeleccionado);
// }

function ValidacionNombre() {

    valor = document.getElementById("nombre").value;

    var espacios = false;
    var cont = 0;

    while (!espacios && (cont < valor.length)) {
        if (valor.charAt(cont) == " ") {
            espacios = true;
        }
        cont++; // Esto sí ocurre 
    }

    if (valor == null) {
        alert("El campo nombre no puede ser nulo");
        return false;
    } else if (valor.length == 0) {
        alert("Tiene que completar el campo nombre");
        return false;
    } else if (/^\s+$/.test(valor.length)) {
        alert("El campo nombre no esta de forma correcta");
        return false;
    } else if (espacios) {
        alert("el nombre no puede contener espacios en blanco");
        return false;
    }
}

function sololetras(valor) {
    valor = document.getElementById("nombre").value;
    key = valor.keyCode || valor.which;

    teclado = String.fromCharCode(key).toLowerCase();

    letras = "qwertyuiopasdfghjklñzxcvbnm ";

    especiales = "8-37-38-46-164";

    teclado_especial = false;

    for (var i in especiales) {
        if (key == especiales[i]) {
            teclado_especial = true;
            break;
        }
    }

    if (letras.indexOf(teclado) == -1 && !teclado_especial) {
        return false;
    }

}

function ApellidoMaterno() {
    valor = document.getElementById("apellidoMaterno").value;

    var espacios = false;
    var cont = 0;

    while (!espacios && (cont < valor.length)) {
        if (valor.charAt(cont) == " ") {
            espacios = true;
        }
        cont++; // Esto sí ocurre 
    }

    if (valor == null) {
        alert("El campo Apellido Materno no puede ser nulo");
        return false;
    } else if (valor.length == 0) {
        alert("Tiene que completar el campo Apellido Materno");
        return false;
    } else if (/^\s+$/.test(valor.length)) {
        alert("El campo Apellido Materno no esta de forma correcta");
        return false;
    } else if (espacios) {
        alert("el Apellido Materno no puede contener espacios en blanco");
        return false;
    }
}

function ApellidoPaterno() {
    valor = document.getElementById("apellidoPaterno").value;

    var espacios = false;
    var cont = 0;

    while (!espacios && (cont < valor.length)) {
        if (valor.charAt(cont) == " ") {
            espacios = true;
        }
        cont++; // Esto sí ocurre 
    }

    if (valor == null) {
        alert("El campo Apellido Paterno no puede ser nulo");
        return false;
    } else if (valor.length == 0) {
        alert("Tiene que completar el campo Apellido Paterno");
        return false;
    } else if (/^\s+$/.test(valor.length)) {
        alert("El campo Apellido Paterno no esta de forma correcta");
        return false;
    } else if (espacios) {
        alert("el Apellido Paterno no puede contener espacios en blanco");
        return false;
    }
}

function validarEntero(valor) {
    //intento convertir a entero. 
    //si era un entero no le afecta, si no lo era lo intenta convertir 
    valor = parseInt(document.getElementById("edad").value);

    //Compruebo si es un valor numérico 
    if (isNaN(valor)) {
        //entonces (no es numero) devuelvo el valor cadena vacia 
        alert("el valor debe ser numerico");
        return false;
    } else {
        //En caso contrario (Si era un número) devuelvo el valor 
        return valor
    }
}

